Simple UI wrapper around the [Petstore public API](https://petstore.swagger.io).

See [here](https://www.taniarascia.com/how-to-connect-to-an-api-with-javascript/) for
using Javascript to make API calls.
